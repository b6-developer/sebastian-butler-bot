package com.adprogb6.sebastian.model.datamodel;

import org.junit.Test;

import static org.junit.Assert.*;

public class GrammarDataResponseTest {

    @Test
    public void synonymDataResponseCanSetAndGetStatusCode() {
        GrammarDataResponse response = new GrammarDataResponse();
        response.setStatus(200);
        assertEquals(200, response.getStatus());
        response.setStatus(404);
        assertEquals(404, response.getStatus());
    }

    @Test
    public void synonymDataResponseCanSetAndGetMessage() {
        GrammarDataResponse response = new GrammarDataResponse();
        response.setMessage("success");
        assertEquals("success", response.getMessage());
        response.setMessage("failed");
        assertEquals("failed", response.getMessage());
    }

    @Test
    public void synonymDataResponseCanSetAndGetResult() {
        GrammarDataResponse response = new GrammarDataResponse();
        response.setResult("Ada, ada, ada");
        assertEquals("Ada, ada, ada", response.getResult());
    }



}