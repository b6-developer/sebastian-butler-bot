package com.adprogb6.sebastian.service.stemmer;

import org.junit.Test;

import static org.junit.Assert.*;

public class StemmerTest {
    Stemmer stemmer = Stemmer.getInstance();

    @Test
    public void canLemmatizeWords() {
        assertEquals("makan", stemmer.lemmatize("memakan"));
        assertEquals("minum", stemmer.lemmatize("meminum"));
    }



}