package com.adprogb6.sebastian.service.help;

import com.adprogb6.sebastian.model.datamodel.action.actionHandler.Help.HelpAction;
import com.adprogb6.sebastian.model.datamodel.action.actionHandler.Help.behaviour.HelperBehaviour;
import com.adprogb6.sebastian.model.datamodel.action.actionHandler.Help.behaviour.TotalHelperBehaviour;

public class HelperActionFactory {

    public HelperBehaviour createHelperAction(String command, String[] content) {
        HelperBehaviour helperBehaviour = null;
        if (content[0].equalsIgnoreCase("all")) {
            helperBehaviour = new TotalHelperBehaviour();
        }
        return helperBehaviour;
    }

}
