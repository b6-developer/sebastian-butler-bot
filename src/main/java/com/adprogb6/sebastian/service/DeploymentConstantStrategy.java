package com.adprogb6.sebastian.service;

public class DeploymentConstantStrategy implements ConstantStrategy {
    @Override
    public String getIndonesiaGrammarUrl() {
        return System.getenv("GRAMMAR_URL_Id");
    }

    @Override
    public String getWeatherAPIKey() {
        return System.getenv("OPENWEATHER_API_KEY");
    }
}
