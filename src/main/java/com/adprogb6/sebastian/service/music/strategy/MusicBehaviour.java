package com.adprogb6.sebastian.service.music.strategy;

public abstract class MusicBehaviour {
    public MusicBehaviour() {

    }
    public abstract String findArtistChart(String countryId);
    public abstract String songLyric(String word);
    public abstract String findTrack(String songTitle);
    public abstract String findMusicChart(String countryId);
}