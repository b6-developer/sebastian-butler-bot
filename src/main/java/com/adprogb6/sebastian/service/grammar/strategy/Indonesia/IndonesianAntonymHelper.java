package com.adprogb6.sebastian.service.grammar.strategy.Indonesia;

import com.adprogb6.sebastian.model.datamodel.GrammarDataResponse;
import com.adprogb6.sebastian.service.ConstantLoader;
import com.adprogb6.sebastian.service.grammar.strategy.AntonymHelper;
import org.springframework.web.client.RestTemplate;

public class IndonesianAntonymHelper implements AntonymHelper {
    RestTemplate restTemplate = new RestTemplate();
    private final String antonymUrl = ConstantLoader.getIndonesiaGrammarUrl()+"antonim/";
    private final String FAILED_RESPONSE = "Maaf tuan, saya tidak bisa menemukan kata yang anda maksud";
    private String response = "";

    @Override
    public String findAntonym(String word) {
        if (word.equalsIgnoreCase("")) {
            return FAILED_RESPONSE;
        }
        return makeAntonymResponse(word);
    }

    private String makeAntonymResponse(String words) {
        GrammarDataResponse antonymResponse =restTemplate.getForObject(antonymUrl +words, GrammarDataResponse.class);
        if (antonymResponse.getStatus()==200) {
            response =antonymResponse.getResult();
        } else {
            response = FAILED_RESPONSE;
        }
        return response;
    }
}
