package com.adprogb6.sebastian.service.grammar.strategy.Indonesia;

import com.adprogb6.sebastian.model.datamodel.GrammarDataResponse;
import com.adprogb6.sebastian.service.ConstantLoader;
import com.adprogb6.sebastian.service.grammar.strategy.SynonymHelper;
import org.springframework.web.client.RestTemplate;

public class IndonesianSynonymHelperMicroservice implements SynonymHelper {
    RestTemplate restTemplate = new RestTemplate();
    private final String synonymUrl = ConstantLoader.getIndonesiaGrammarUrl()+"sinonim/";
    private final String FAILED_RESPONSE = "Maaf tuan, saya tidak bisa menemukan kata yang anda maksud";
    private String response = "";

    @Override
    public String findSynonym(String words) {
        if (words.equalsIgnoreCase("")) {
            return FAILED_RESPONSE;
        }
        return makeSynonymResponse(words);
    }

    private String makeSynonymResponse(String words) {
        GrammarDataResponse synonymResponse =restTemplate.getForObject(synonymUrl+words, GrammarDataResponse.class);
        if (synonymResponse.getStatus()==200) {
            response =synonymResponse.getResult();
        } else {
            response = FAILED_RESPONSE;
        }
        return response;
    }
}
