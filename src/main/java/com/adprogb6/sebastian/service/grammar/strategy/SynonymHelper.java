package com.adprogb6.sebastian.service.grammar.strategy;

public interface SynonymHelper {
    public String findSynonym(String words);
}
