package com.adprogb6.sebastian.service.grammar.strategy.Indonesia;

import com.adprogb6.sebastian.service.grammar.strategy.SynonymHelper;
import com.adprogb6.sebastian.service.stemmer.Stemmer;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class IndonesianSynonymHelperKamusLengkap implements SynonymHelper {
    private Stemmer stemmer = Stemmer.getInstance();
    private final String SINONIM_URL = "https://kamuslengkap.com/kamus/sinonim/arti-kata/";
    private String response = "";

    @Override
    public String findSynonym(String words) {
        String stemmedWord =stemmer.lemmatize(words);
        try {
            Iterator<Element> tableContent = getTableContentData(stemmedWord);
            CreateSynonymResponse(tableContent);
        } catch (IOException e) {
            response = "Maaf tuan, saya tidak bisa menemukan kata yang anda maksud";
            System.err.println("Error when getting url data");
        }
        return response;

    }

    private void CreateSynonymResponse(Iterator<Element> tableContent) {
        tableContent.next();
        String wordName = tableContent.next().text();
        tableContent.next();
        String synonym  = tableContent.next().text();
        response = createResponseFromList(cleanSynonym(synonym));
    }

    private Iterator<Element> getTableContentData(String stemmedWord) throws IOException {
        Document document = Jsoup.connect(SINONIM_URL+stemmedWord).get();

        Element table = document.select("table").first();
        return table.select("td").iterator();
    }

    private String createResponseFromList(List<String> words) {
        StringBuilder responseCreator = new StringBuilder();
        for (String word : words) {
            responseCreator.append(word);
            responseCreator.append(" ");
        }
        return responseCreator.toString();
    }

    private List<String> cleanSynonym(String rawSynonym) {
        List<String> synonymWordList = Arrays.asList(rawSynonym.split(" "));
        List<String> cleanedWords = createWithoutDigitsCharList(synonymWordList);
        List<String> toRemoveWords = getUnRequiredWords(cleanedWords);
        RemoveUnRequiredWords(cleanedWords, toRemoveWords);
        System.err.println("Setelah dibersihkan...");
        cleanedWords.forEach(System.err::println);
        return cleanedWords;


    }

    private void RemoveUnRequiredWords(List<String> cleanedWords, List<String> toRemoveWords) {
        for(String toRemove : toRemoveWords) {
            cleanedWords.remove(toRemove);
        }
    }

    private List<String> getUnRequiredWords(List<String> cleanedWords) {
        List<String> removedwords = new ArrayList<>();
        for (int x =0 ; x < cleanedWords.size()  ; x++) {
            String words = cleanedWords.get(x).replace(" ","");
            if (isUnRequiredWords(words)) {
                removedwords.add(cleanedWords.get(x));
            }
        }
        return removedwords;
    }

    private boolean isUnRequiredWords(String words) {
        boolean removeFlag = false;
        if (words.equalsIgnoreCase("ant") ||
        words.equalsIgnoreCase("setem")||
        words.equalsIgnoreCase("--")) {
            removeFlag = true;
        }
        return removeFlag;
    }

    private List<String> createWithoutDigitsCharList(List<String> wordsList) {
        List<String> cleanedWords = new ArrayList<>();
        wordsList.stream()
                .filter(s ->!isADigit(s) )
                .forEach(cleanedWords::add);
        return cleanedWords;
    }

    private boolean isADigit(String character) {
        char characterTest = character.charAt(0);
        return Character.isDigit(characterTest);
    }



}
