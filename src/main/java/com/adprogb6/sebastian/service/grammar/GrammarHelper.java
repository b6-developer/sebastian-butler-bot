package com.adprogb6.sebastian.service.grammar;

import com.adprogb6.sebastian.service.grammar.strategy.GrammarBehaviour;
import com.adprogb6.sebastian.service.grammar.strategy.Indonesia.IndonesianGrammarBehaviour;


public class GrammarHelper {
    private GrammarBehaviour grammarBehaviour;


    public GrammarHelper(GrammarBehaviour grammarBehaviour) {
        this.grammarBehaviour = grammarBehaviour;
    }

    public GrammarHelper() {
        grammarBehaviour = new IndonesianGrammarBehaviour();
    }

    //@TODO implements
    public boolean isStandard(String word) { return grammarBehaviour.isStandard(word); }
    public String findSynonym(String word) {
        return grammarBehaviour.findSynonym(word);
    }

    public String findAntonym(String word) {
        return grammarBehaviour.findAntonym(word);
    }
    public String findMeaning(String word) {
        return grammarBehaviour.findMeaning(word);
    }
}
