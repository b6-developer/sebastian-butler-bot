package com.adprogb6.sebastian.service.grammar;

public class GrammarUtil {

    public static String capitalize(String text) {
        text = text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
        return text;
    }

    public static String convertToString(String[] userInput) {
        StringBuilder userInputBuilder = new StringBuilder();
        for (int indeks = 0; indeks< userInput.length; indeks++) {
            userInputBuilder.append(userInput[indeks]);
            userInputBuilder.append(" ");
        }
        return userInputBuilder.toString();
    }
}
