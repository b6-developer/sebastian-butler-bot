package com.adprogb6.sebastian.service.news.strategy;

import com.adprogb6.sebastian.model.datamodel.news.NewsDataResponse;
import com.adprogb6.sebastian.service.news.NewsBehaviour;

import org.springframework.web.client.RestTemplate;

public class KeywordNews implements NewsBehaviour {
    RestTemplate restTemplate = new RestTemplate();
    private static String TOP_HEADLINE_URL_1 = "https://newsapi.org/v2/everything?q=";
    private static String TOP_HEADLINE_URL_2 = "&from=2019-04-23&sortBy=popularity&apiKey=";
    private static final String API_KEY = "4500c1bc77e14a0cad32f2c0461fd0b9";
    private static final String FAILED_RESPONSE = "Maaf tuan, saya tidak bisa menemukan berita yang anda maksud";

    public String findNews(String words) {
        if (words.equalsIgnoreCase("")) {
            return FAILED_RESPONSE;
        }

        return makeNewsResponse(words);
    }

    public String makeNewsResponse(String words) {
        StringBuilder response = new StringBuilder();
        NewsDataResponse newsDataResponse = restTemplate.getForObject(TOP_HEADLINE_URL_1 + words + TOP_HEADLINE_URL_2 + API_KEY, NewsDataResponse.class);

        response.append(String.format("[%s]\n\n", newsDataResponse.getArticlesDataResponse()[0].getTitle()));
        response.append(String.format("%s\n\n", newsDataResponse.getArticlesDataResponse()[0].getContent()));
        response.append(String.format("Author: %s\n", newsDataResponse.getArticlesDataResponse()[0].getAuthor()));
        response.append(String.format("Source: %s", newsDataResponse.getArticlesDataResponse()[0].getSource().getName()));

        return response.toString();

    }
    
}