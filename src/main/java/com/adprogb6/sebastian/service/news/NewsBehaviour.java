package com.adprogb6.sebastian.service.news;

public interface NewsBehaviour {
    public String findNews(String words);
}