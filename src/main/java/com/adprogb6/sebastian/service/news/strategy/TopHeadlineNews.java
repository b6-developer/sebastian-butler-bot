package com.adprogb6.sebastian.service.news.strategy;

import com.adprogb6.sebastian.model.datamodel.news.NewsDataResponse;
import com.adprogb6.sebastian.service.news.NewsBehaviour;

import org.springframework.web.client.RestTemplate;

public class TopHeadlineNews implements NewsBehaviour {
    RestTemplate restTemplate = new RestTemplate();
    private static final String TOP_HEADLINE_URL = "https://newsapi.org/v2/top-headlines?country=id&apiKey=";
    private static final String API_KEY = "4500c1bc77e14a0cad32f2c0461fd0b9";
    private static final String FAILED_RESPONSE = "Maaf tuan, saya tidak bisa menemukan berita yang anda maksud";

    public String findNews(String words) {
        StringBuilder response = new StringBuilder();
        NewsDataResponse newsDataResponse = restTemplate.getForObject(TOP_HEADLINE_URL + API_KEY, NewsDataResponse.class);

        if (newsDataResponse.getStatus().equals("ok")) {
            response.append(String.format("[%s]\n\n", newsDataResponse.getArticlesDataResponse()[0].getTitle()));
            response.append(String.format("%s\n\n", newsDataResponse.getArticlesDataResponse()[0].getContent()));
            response.append(String.format("Author: %s\n", newsDataResponse.getArticlesDataResponse()[0].getAuthor()));
            response.append(String.format("Source: %s", newsDataResponse.getArticlesDataResponse()[0].getSource().getName()));
        } else {
            response.append(FAILED_RESPONSE);
        }

        return response.toString();

    }
    
}