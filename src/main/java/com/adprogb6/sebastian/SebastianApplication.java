package com.adprogb6.sebastian;

import com.adprogb6.sebastian.model.datamodel.action.actionFactory.ActionFactory;
import com.adprogb6.sebastian.model.datamodel.action.actionHandler.ActionHandler;
import com.adprogb6.sebastian.service.InputProcessing.InputService;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.util.concurrent.ExecutionException;

@SpringBootApplication
public class SebastianApplication extends SpringBootServletInitializer {




    public static void main(String[] args) { SpringApplication.run(SebastianApplication.class, args); }

}
