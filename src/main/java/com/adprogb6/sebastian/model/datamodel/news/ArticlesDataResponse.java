package com.adprogb6.sebastian.model.datamodel.news;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ArticlesDataResponse implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @JsonProperty("source")
    private SourceDataResponse sourceDataResponse;

    @JsonProperty("author")
    private String author;

    @JsonProperty("title")
    private String title;

    @JsonProperty("description")
    private String description;

    @JsonProperty("url")
    private String url;

    @JsonProperty("urlToImage")
    private String urlToImage;

    @JsonProperty("content")
    private String content;

    public void setSource(SourceDataResponse sourceDataResponse) {
        this.sourceDataResponse = sourceDataResponse;
    }

    public SourceDataResponse getSource() {
        return sourceDataResponse;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrlToImage(String urlToImage) {
        this.urlToImage = urlToImage;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}