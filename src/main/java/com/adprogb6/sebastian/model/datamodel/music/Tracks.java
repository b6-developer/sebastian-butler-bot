package com.adprogb6.sebastian.model.datamodel.music;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Tracks {
    private Track[] track;

    public Track[] getTrack() {
        return track;
    }

    public void setTracks(Track[] track) {
        this.track = track;
    }
}
