package com.adprogb6.sebastian.model.datamodel.news;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class SourceDataResponse implements Serializable {
    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}