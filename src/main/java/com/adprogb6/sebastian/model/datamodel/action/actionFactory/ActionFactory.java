package com.adprogb6.sebastian.model.datamodel.action.actionFactory;

import com.adprogb6.sebastian.model.datamodel.action.actionHandler.ActionHandler;
import com.adprogb6.sebastian.model.datamodel.action.actionHandler.Butler.ButlerAction;
import com.adprogb6.sebastian.model.datamodel.action.actionHandler.Grammar.GrammarAction;
import com.adprogb6.sebastian.model.datamodel.action.actionHandler.Help.HelpAction;
import com.adprogb6.sebastian.model.datamodel.action.actionHandler.Music.MusicAction;
import com.adprogb6.sebastian.model.datamodel.action.actionHandler.News.NewsAction;

public class ActionFactory {


    public ActionHandler createAction(String identifier, String command, String[] content) {
        ActionHandler actionHandler = null;
        if (identifier.equalsIgnoreCase("cari")) {
            actionHandler = new GrammarAction(command, content);
        } else if (identifier.equalsIgnoreCase("tolong")) {
            actionHandler = new ButlerAction(command, content);
        } else  if(identifier.equalsIgnoreCase("berikan")) {
            actionHandler = new MusicAction(command, content);
        } else if(identifier.equalsIgnoreCase("tampilkan")) {
            actionHandler = new NewsAction(command, content);
        }else {
            actionHandler = new HelpAction(command, content);
        }
        return actionHandler;

    }

}
