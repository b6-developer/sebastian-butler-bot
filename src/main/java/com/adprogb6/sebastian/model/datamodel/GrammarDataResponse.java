package com.adprogb6.sebastian.model.datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class GrammarDataResponse implements Serializable {
    @JsonProperty("status")
    private int status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("Result")
    private String result;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}


