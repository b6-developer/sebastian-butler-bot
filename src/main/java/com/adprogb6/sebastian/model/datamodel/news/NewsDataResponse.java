package com.adprogb6.sebastian.model.datamodel.news;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class NewsDataResponse implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @JsonProperty("status")
    private String status;

    @JsonProperty("totalResults") 
    private int totalResults;

    @JsonProperty("articles")
    private ArticlesDataResponse[] articlesDataResponse;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setArticlesDataResponse(ArticlesDataResponse[] articlesDataResponse) {
        this.articlesDataResponse = articlesDataResponse;
    }

    public ArticlesDataResponse[] getArticlesDataResponse() {
        return articlesDataResponse;
    }
}