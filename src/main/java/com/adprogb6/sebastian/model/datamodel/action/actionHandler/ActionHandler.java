package com.adprogb6.sebastian.model.datamodel.action.actionHandler;

public abstract class ActionHandler {
    private String command;
    private String[] content;

    private String headerContent;
    private String textMessageResponse;
    private String imageUrlContent;

    private final String FAILED_RESPONSE = "Maafkan saya tuan, saya tidak memahami" +
            "yang tuan katakan";

    public ActionHandler(String command, String[] content) {
        this.command = command;
        this.content = content;
        init();
        handleInput(command, content);
    }
    public abstract String getTextMessageContent();

    public abstract String getHeaderContent();

    public abstract String getImageUrlContent();

    public abstract void init();

    protected abstract void handleInput(String command, String[] content);

    protected String getCommand() {
        return command;
    }

    protected void setCommand(String command) {
        this.command = command;
    }

    protected String[] getContent() {
        return content;
    }

    protected void setContent(String[] content) {
        this.content = content;
    }

    protected void setHeaderContent(String headerContent) {
        this.headerContent = headerContent;
    }

    protected void setTextMessageResponse(String textMessageResponse) {
        this.textMessageResponse = textMessageResponse;
    }

    protected void setImageUrlContent(String imageUrlContent) {
        this.imageUrlContent = imageUrlContent;
    }

    protected String getFAILED_RESPONSE() {
        return FAILED_RESPONSE;
    }

    protected String getTextMessageResponse() {
        return textMessageResponse;
    }
}
