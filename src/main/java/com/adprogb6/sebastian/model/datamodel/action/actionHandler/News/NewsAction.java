package com.adprogb6.sebastian.model.datamodel.action.actionHandler.News;

import com.adprogb6.sebastian.model.datamodel.action.actionHandler.ActionHandler;
import com.adprogb6.sebastian.service.news.strategy.TopHeadlineNews;

public class NewsAction extends ActionHandler {

    private static final String FAILED_RESPONSE = "Maaf tuan, saya tidak bisa menampilkan berita tersebut";
    private TopHeadlineNews topHeadlineNews;

    public NewsAction(String command, String[] content) {
        super(command, content);
    }

    @Override
    public void init() {
        topHeadlineNews = new TopHeadlineNews();
    }

    @Override
    public String getTextMessageContent() {
        return getTextMessageResponse();
    }   

    @Override
    public String getHeaderContent() {
        return null;
    }

    @Override
    public String getImageUrlContent() {
        return null;
    }

    @Override
    protected void handleInput(String command, String[] content) {
        if (command.equals("top-headlines")) {
            handleTopHeadlinesCommand();
        } else if (command.equals("berita")) {

        } else if (command.equals("kategori")) {

        } else {
            setTextMessageResponse(NewsAction.FAILED_RESPONSE);
        }
    }

    private void handleTopHeadlinesCommand() {
        StringBuilder responseBuilder = new StringBuilder();

        responseBuilder.append(topHeadlineNews.findNews(""));
        setTextMessageResponse(responseBuilder.toString());
    }
}
