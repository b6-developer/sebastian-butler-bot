package com.adprogb6.sebastian.model.datamodel.cuaca;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Cuaca {

    private Weather[] weather;
    private Main main;

    public Cuaca() {
    }

    public Weather[] getWeather() {
        return weather;
    }

    public void setWeather(Weather[] weather) {
        this.weather = weather;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public String getDescription() {
        return idToDescription.translateId(weather[0].getId());
    }

    public double getTemp() {
        return main.getTemp();
    }

    public int getHumidity() {
        return main.getHumidity();
    }
}
