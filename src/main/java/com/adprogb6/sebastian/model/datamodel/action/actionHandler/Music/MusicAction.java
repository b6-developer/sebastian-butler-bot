package com.adprogb6.sebastian.model.datamodel.action.actionHandler.Music;

import com.adprogb6.sebastian.model.datamodel.action.actionHandler.ActionHandler;
import com.adprogb6.sebastian.model.datamodel.music.*;
import com.adprogb6.sebastian.service.music.strategy.MusicHelper;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.util.Scanner;

import java.io.IOException;

public class MusicAction extends ActionHandler {
    private MusicHelper musicHelper;
    private final String failResponse = "Maaf tuan, saya tidak bisa menemukan yang anda cari";

    public MusicAction(String command, String[] content) {
        super(command, content);
    }

    @Override
    public void init() {
        musicHelper = new MusicHelper();
    }

    @Override
    protected void handleInput(String command, String[] content) {
        if (command.equalsIgnoreCase("top-artist")) {
            handleArtistChart(Integer.parseInt(content[0]));
        }
        else if (command.equalsIgnoreCase("lirik-lagu")) {
            handleFindLyric(content);
        }
        else if (command.equalsIgnoreCase("top-songs")) {
            handleMusicChart(Integer.parseInt(content[0]));
        }
        else if (command.equalsIgnoreCase("informasi")) {
            handleArtistInfo(content);
        }
    }


    @Override
    public String getTextMessageContent() {
        return getTextMessageResponse();
    }

    @Override
    public String getHeaderContent() {
        return null;
    }

    @Override
    public String getImageUrlContent() {
        return null;
    }

    private void handleArtistInfo(String[] artistInfo) {
        StringBuilder responBuilder = new StringBuilder();
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://ws.audioscrobbler.com/2.0?method=artist.getinfo&artist=";
        for (int i = 0; i<artistInfo.length; i++) {
            if (i+1 == artistInfo.length) {
                url += artistInfo[i];
            }
            else url += artistInfo[i] + "%20";
        }
        url += "&api_key=e7934e8e37a60cc56fc8d647f5c0036e&format=json";
        System.err.print(url);
        try {
            Music music = restTemplate.getForObject(url, Music.class);
            String summary = music.getArtist().getBio().getSummary();
            summary = summary.substring(0, summary.indexOf("<a"));
            responBuilder.append("Baik tuan, berikut informasi terkait artis yang anda cari yang saya temukan:\n");
            responBuilder.append(summary);
            setTextMessageResponse(responBuilder.toString());
        }
        catch (HttpClientErrorException e) {
            setTextMessageResponse("Error saat mencari informasi");
        }
        catch (NullPointerException e) {
            setTextMessageResponse("Tidak ditemukan informasi artis yang dicari");
        }
    }
    private void handleArtistChart(int jumlahArtist) {
        StringBuilder responBuilder = new StringBuilder();
        String url = "http://ws.audioscrobbler.com/2.0/?method=chart.gettopartists&api_key=e7934e8e37a60cc56fc8d647f5c0036e&format=json&limit="+jumlahArtist;
        RestTemplate restTemplate = new RestTemplate();
        int counter = 1;
        try {
            Music music = restTemplate.getForObject(url, Music.class);
            responBuilder.append("Baik tuan, berikut chart artist yang saya dapatkan: \n");
            for (Artist artist: music.getArtists().getArtist()) {
                if (counter == jumlahArtist) {
                    responBuilder.append(counter + ". " + artist.getName());
                }
                else {
                    responBuilder.append(counter + ". " + artist.getName() + "\n");
                }
                counter++;
            }
        setTextMessageResponse(responBuilder.toString());
        }
        catch (HttpClientErrorException e) {
            setTextMessageResponse("Maaf tuan, terjadi kesalahan, pastikan input benar dan ulang kembali");
        }
        System.err.println(responBuilder.toString());
    }

    private void handleMusicChart(int songsAmount) {
        StringBuilder responBuilder = new StringBuilder();
        String url = "http://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key=e7934e8e37a60cc56fc8d647f5c0036e&format=json&limit="+songsAmount;
        RestTemplate restTemplate = new RestTemplate();
        int counter = 1;
        try {
            MusicChart musicChart = restTemplate.getForObject(url, MusicChart.class);
            responBuilder.append("Baik tuan, berikut chart lagu yang saya dapatkan: \n");
            for (Track track: musicChart.getTracks().getTrack()) {
                if (counter == songsAmount) {
                    responBuilder.append(counter + ". " + track.getName() + " by " + track.getArtist().getName());
                }
                else {
                    responBuilder.append(counter + ". " + track.getName() + " by " + track.getArtist().getName() + "\n");
                }
                counter++;
            }
            setTextMessageResponse(responBuilder.toString());
        }
        catch (HttpClientErrorException e) {
            setTextMessageResponse("Maaf tuan, terjadi kesalahan, pastikan input benar dan ulang kembali");
        }
        System.err.println(responBuilder.toString());
    }

    public void handleFindLyric(String[] query) {
        String lyric = "";
        StringBuilder responseBuilder = new StringBuilder();
        String accessToken = "0Td-aW8D_qtGh26saZ4cnIvkt1Yj9D4iNyJ7QBxo17bxHP2e-6otvtNlgDB_nC9o";
        String API_URL = "https://api.genius.com/search?q=";
        for (String q: query) {
            API_URL += q + " ";
        }
        RestTemplate restTemplate = new RestTemplate();
        try {
            Genius genius = restTemplate.getForObject(API_URL+"&access_token="+accessToken, Genius.class);
            if (genius.getResponse().getHits().length == 0) {
                setTextMessageResponse("Maaf tuan, saya tidak bisa menemukan lirik lagu yang anda cari");
                return;
            }

            String lyric_url = genius.getResponse().getHits()[0].getResult().getUrl();
            try {
                Document document = Jsoup.connect(lyric_url).get();
                document.outputSettings().prettyPrint(false);

                Elements elements = document.getElementsByClass("lyrics");
                for (Element e : elements) {
                    e.select("br").append("\\n");
                    e.select("p").prepend("\\n\\n");
                    lyric+=e.text();
                }
                if (lyric.length() <= 2000) {
                    responseBuilder.append(lyric.replaceAll("\\\\n", "\n"));
                }
                else {
                    String selengkapnya = "\nUntuk lirik lebih lengkap, kunjungi: \n" + lyric_url;
                    lyric = lyric.substring(4, 1999-selengkapnya.length()-5);
                    lyric+= selengkapnya;
                    responseBuilder.append(lyric.replaceAll("\\\\n", "\n"));
                }
                setTextMessageResponse(responseBuilder.toString());

            }
            catch (IOException e) {
                System.err.println("Error when getting lyrics");
                setTextMessageResponse("Gagal mendapatkan lirik lagu yang diminta");
            }
        }
        catch (HttpClientErrorException e) {
            setTextMessageResponse("Maaf tuan, terjadi kesalahan, mohon pastikan input benar dan coba lagi");
        }


    }
}
