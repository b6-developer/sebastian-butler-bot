package com.adprogb6.sebastian.controller;


import com.adprogb6.sebastian.model.datamodel.action.actionFactory.ActionFactory;
import com.adprogb6.sebastian.model.datamodel.action.actionHandler.ActionHandler;
import com.adprogb6.sebastian.service.InputProcessing.InputService;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.ExecutionException;


@LineMessageHandler
public class MainController {
    private ActionFactory actionFactory = new ActionFactory();

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private InputService inputService;

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent) {
        TextMessageContent input = messageEvent.getMessage();
        responseUser(input.getText(),messageEvent.getReplyToken());
    }

    private void responseUser(String userMessage, String replyToken) {
        inputService.takeInput(userMessage);
        if (inputService.isACommand()) {
            sendMessageToUser(replyToken, getMessageResponse());
        }

    }

    private TextMessage getMessageResponse() {
        TextMessage response;
        if (inputService.isValidInput()) {
            ActionHandler actionHandler  = actionFactory.createAction(inputService.getIdentifier(),
                    inputService.getCommand(), inputService.getContent());
            response = new TextMessage(actionHandler.getTextMessageContent());
        } else {
            response = new TextMessage(inputService.getCommand());
        }
        return response;
    }

    private void sendMessageToUser(String replyToken, TextMessage response) {
        try {
            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, response))
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Error when replying message");
        }
    }



}
